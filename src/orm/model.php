<?php

namespace testProject\orm;

abstract class Model
{

    /**
     * name of collection 
     */
    protected  $collection;

    /**
     * model data
     */
    private $data;
  

    function  __construct($collection){
        $this->collection=$collection;
    }
    private function loadData(){
        $string = file_get_contents("./data.json");
        $data = json_decode($string, true);  
        $this->data=$data[$this->collection]??[];
    }
    protected function  get(){
        $this->loadData();
        return $this->data;
    }

    protected function  find($id){
        $this->loadData();

       foreach($this->data as $element){
           if($element['id']==$id) return $element;
       }
       return null;
    }

    protected function  findByField($fieldName,$value){
    
        $this->loadData();
        $result=[];

       foreach($this->data as $element){
           if($element[$fieldName]==$value)  $result[]=$element;
       }
       return $result;
    }



}
