<?php

namespace testProject\finance\accounts;

use testProject\orm\Model;
use  testProject\finance\transactions\Transaction;
class  Account extends Model{
  
    
    /**
     *    properties:
     *   id,name,currency, description
     */

    function __construct() {
       
        parent::__construct("accounts");
    }

    /**
     *  return account list 
     */
    public  function list() {
		return $this->get();
	}


     /**
     *  return account balance
     *  Params :$id :  account id 
     */
    public  function accountBalance($id) {

        $transaction= new Transaction();
        $accountTransactions= $transaction->accountTransactions($id);

        $debits=0;
        $credits=0;
        foreach( $accountTransactions as $transaction){
             $debits+=$transaction['debit'];
             $credits+=$transaction['credit'];
        }
        return $credits- $debits;
	
	}


     /**
     *  return account transactions sorted by comment
     * Params :$id :  account id 
     */
    public  function accountTransactionByComment($id) {

        $transaction= new Transaction();
        $accountTransactions= $transaction->accountTransactions($id);
        usort( $accountTransactions, function ($a, $b) {
            return strcmp($a["comment"], $b["comment"]);
        });
     
        return $accountTransactions;
	
	}

    /**
     *  return account transactions sorted by date
     * Params :$id :  account id 
     */
    public  function accountTransactionByDate($id) {
        $transaction= new Transaction();
        $accountTransactions= $transaction->accountTransactions($id);
        usort( $accountTransactions, function ($a, $b) {
            return $a["date"]< $b["date"];
        });
        return $accountTransactions;
	
	}
  







}