# Task PHP

## Getting Started

Clone this repository and then run Composer:-

```
composer install
```

You will then be able to run the unit tests using:

```
./vendor/bin/phpunit
```

Data Sample :

```
{
    "accounts":[
        {
            "id":1,
            "currency":"USD",
            "name":"account1"
        },
        {
            "id":2,
            "currency":"USD",
            "name":"account2"
        },
        {
            "id":3,
            "currency":"USD",
            "name":"account3"
        }
    ],
    "transactions":[
        {
            "id":1,
            "accountId":1,
            "type":"deposit",
            "comment":"deposit by machine # 4",
            "credit":200,
            "debit":0,
            "date":"2022-04-27 10:00:00"
        },
        {
            "id":2,
            "accountId":1,
            "type":"transfer",
            "comment":"transfer from account1",
            "credit":0,
            "debit":50,
            "date":"2022-04-28 10:00:00"
        }
        ,
        {
            "id":3,
            "accountId":2,
            "type":"transfer",
            "comment":"transfer to account2",
            "credit":50,
            "debit":0,
            "date":"2022-04-28 10:00:01"
        },
        {
            "id":4,
            "accountId":2,
            "type":"withdrawal",
            "comment":"withdrawal from account2",
            "credit":0,
            "debit":25,
            "date":"2022-04-29 10:00:00"
        }
    ]
}

```
