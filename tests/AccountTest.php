<?php


use PHPUnit\Framework\TestCase;
use testProject\finance\accounts\account;
class AccountTest extends TestCase
{
    

    public function setUp()
    {
        $this->account = new Account();
    }


    public function testAccountsCount()
    {     

        $this->assertEquals(3,count( $this->account->list()));
    }

    public function testAccountsBalance()
    {     
    

        $this->assertEquals(150,$this->account->accountBalance(1));
        $this->assertEquals(25,$this->account->accountBalance(2));
        $this->assertEquals(0,$this->account->accountBalance(3));
    }

    public function testAccountTransactionByComment()
    {   
          
        $this->assertEquals("deposit by machine # 4",$this->account->accountTransactionByComment(1)[0]["comment"]); 
    }

    public function testAccountTransactionByDate()
    {     
        $this->assertEquals("2022-04-28 10:00:00",$this->account->accountTransactionByDate(1)[0]["date"]); 
    }

  

 
}
